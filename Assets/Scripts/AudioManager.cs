﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// takes care of all audio in the game
/// </summary>
public class AudioManager : MonoBehaviour {

    public Sound[] sounds;  //list of available sounds
    private static AudioManager instance;

    /// <summary>
    /// int the beginning we make this a "singleton" and create audio source for all items in sounds list
    /// </summary>
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.loop = s.loop;
        }
    }

    private void Start() //we start looping the theme music
    {
        Play("Theme");
    }

    /// <summary>
    /// Call with audio name to play it
    /// </summary>
    /// <param name="audioName"></param>
    public static void Play(string audioName)  
    {
        Sound s = Array.Find(instance.sounds, sound => sound.name == audioName);
        if (s != null)
            s.source.Play();
        else
            Debug.LogWarning("Didnt find " + audioName + "from sounds!");
    }
}
