﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    [HideInInspector]
    public GameManager.BlockType blockType; //our type as a block

    private List<GameObject> collisions = new List<GameObject>(); //the list of nodes we are currently colliding with
    private Vector3 rotation = new Vector3(0, 0, 90); //rotation per rotation
    private GameManager gameManager; 
     
    private bool isDescending = true; //are we able to go down
    private bool isMoveable = true; //is player able to move us

    private float descendingTimer;
    private float currentSpeed;     //speed that we move
    private float originalSpeed;    //the speed that we move as non modified

    /// <summary>
    /// initialize the block. must be called before block is able to do anything
    /// </summary>
    /// <param name="_gameManager"></param>
    /// <param name="_speed"></param>
    /// <param name="_blockType"></param>
    public void Init(GameManager _gameManager,  float _speed, GameManager.BlockType _blockType)
    {
        gameManager = _gameManager;
        originalSpeed = 1 / _speed;
        blockType = _blockType;
        currentSpeed = originalSpeed;

        descendingTimer = Time.time + currentSpeed;
    }

    /// <summary>
    /// we check for user input here
    /// </summary>
    private void Update()
    {
        if(isMoveable)
        {
            if (Input.GetKey(KeyCode.DownArrow))
                currentSpeed = originalSpeed / 10;
            else
                currentSpeed = originalSpeed;

            UpdateCollisionData();

            if (Input.GetKeyDown(KeyCode.RightArrow) && gameManager.CheckMovement(collisions, Vector2Int.right))
                transform.Translate(Vector2.right * 0.29f, Space.World);
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && gameManager.CheckMovement(collisions, Vector2Int.left))
                transform.Translate(Vector2.left * 0.29f, Space.World);

            if (Input.GetKeyDown(KeyCode.Z))
                RotateBlock(true);
            else if (Input.GetKeyDown(KeyCode.X))
                RotateBlock(false);

            if (isDescending && descendingTimer <= Time.time)
            {
                transform.Translate(Vector2.down * 0.29f, Space.World);
                descendingTimer = Time.time + currentSpeed;
            }
        }
    }
    /// <summary>
    /// This will stop the movement of the block, if the user does not move the block out of collisions within given timeframe
    /// </summary>
    /// <returns></returns>
    public IEnumerator StopMoving()
    {
        if(isMoveable && isDescending) //if able
        {
            isDescending = false; //we set our descending to halt
            yield return new WaitForSeconds(currentSpeed);  //then we give user time
            UpdateCollisionData(); //after we recheck our collision data

            for (int i = 0; i < collisions.Count; i++) 
            {
                if (collisions[i].GetComponent<Node>().isActive) 
                    break;
                else if (i == collisions.Count - 1)//and if none of the collisions is active
                    isDescending = true; //we reset the descending
            }
            if(!isDescending)
                isMoveable = false;

            if (!isMoveable && !isDescending) 
                gameManager.BlockDoneFalling(collisions); //if we did still collide with nodes, we tell gamemanager that this block is done
        }
    }

    /// <summary>
    /// Rotates the block either clockwise or counter clockwise 90 decrees
    /// </summary>
    /// <param name="rotateClockwise"></param>
    private void RotateBlock(bool rotateClockwise)
    {
        int dir = 0;
        dir = rotateClockwise ? 1 : -1; //make a integer value of 1 or -1 from the given boolean

        if (blockType == GameManager.BlockType.I || blockType == GameManager.BlockType.S || blockType == GameManager.BlockType.Z) //depending on our block type we either rotate only either to 90 or to 0
            TwoWayRotation();

        else if (blockType == GameManager.BlockType.J || blockType == GameManager.BlockType.L || blockType == GameManager.BlockType.T) //or we can do full circles
            FullRotation(dir);
    }

    /// <summary>
    /// Rotate object from 0 to 90 decrees and from 90 decress back to 0
    /// </summary>
    private void TwoWayRotation()
    {
        if (Mathf.RoundToInt(Mathf.Abs(transform.rotation.eulerAngles.z)) == 0 && gameManager.CheckRotation(collisions, 1))
            transform.Rotate(rotation, Space.World);
        else if (Mathf.RoundToInt(Mathf.Abs(transform.rotation.eulerAngles.z)) == 90 && gameManager.CheckRotation(collisions, -1))
            transform.Rotate(-rotation, Space.World);
    }

    /// <summary>
    /// Rotate object 90 decrees to the dir each time
    /// </summary>
    /// <param name="dir"></param>
    private void FullRotation(int dir)
    {
        if (gameManager.CheckRotation(collisions, dir))
            transform.Rotate(rotation * dir, Space.World);
    }

    /// <summary>
    /// gathers data from the children if and what they are colliding with
    /// </summary>
    private void UpdateCollisionData()
    {
        collisions.Clear();
        foreach (Transform child in transform)
            collisions.Add(child.GetComponent<BlockListener>().GetCollision());
    }
}
