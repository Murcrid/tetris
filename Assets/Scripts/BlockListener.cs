﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockListener : MonoBehaviour {

    private GameObject collidingGameobject; //the object we are colliding with

    /// <summary>
    /// ever time we enter a new trigger we run this
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collidingGameobject = collision.gameObject; //set the new colliding object to be the one we collided with

        if (collision.CompareTag("Block") && collision.GetComponent<Node>().isActive) //if it was block and it is active for collisions
        {
            Debug.Log(collision.GetComponent<Node>().positionInGrid);
            StartCoroutine(GetComponentInParent<Block>().StopMoving()); //we tell the block that we are a child of, to stop moving
        }
    }

    /// <summary>
    /// returns the object we are colliding with 
    /// </summary>
    /// <returns></returns>
    public GameObject GetCollision()
    {
        return collidingGameobject;
    }
}
