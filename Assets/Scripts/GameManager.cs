﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Gamemanager class
/// </summary>
public class GameManager : MonoBehaviour {

    [Header("Nodes")]
    public GameObject nodePrefab; //prefab of the node to create the layout from
    public Transform nodeParent;  //the parent object for the nodes

    [Header("Blocks")]
    public List<GameObject> blocks = new List<GameObject>(); //list of the available blocks
    public Sprite blockBlockedSprite; //sprite to use to mark the space in the map as "used"
    public Transform blockParent; //parent object for the blocks
    public Transform blockStart; //where blocks start falling
    public int blockSpeed; //speed that the blocks fall (blockSpeed times per second)

    [Header("UI")]
    public GameObject mainMenu; //menu panel
    public Text scoreText;  //text to show scores
    public Text nextBlockText; //text to show what block will be next

    public int Score { get { return _Score; } set { _Score = value; scoreText.text = _Score.ToString(); } } // holds score, updates thext each time it is updated
    private int _Score;

    public enum BlockType { I, J, L, O, S, T, Z }   //available blocks as preset values

    private List<List<GameObject>> nodes = new List<List<GameObject>>(); //holds the list of the nodes on the grid
    private Block currentBlock; //the block that is currently falling

    private int mapSizeX = 10; //how many nodes is on the map in the x axis
    private int mapSizeY = 23; //how many nodes is on the map in the y axis
    private int nextBlockIndex = -1; //the next block that will fall, has this index(starts from -1 so the first time it is ran, a random one will be generated

    /// <summary>
    /// Starts the game
    /// </summary>
    public void ButtonClick_StartGame()
    {
        CreateMap(); 
        CreateNewRandomBlock();
        mainMenu.SetActive(false); //disable the mainmenu object
        AudioManager.Play("ButtonClick");
    }
    /// <summary>
    /// Exists applicaiton
    /// </summary>
    public void ButtonClick_QuitGame()
    {
        Application.Quit();
    }
    /// <summary>
    /// Called when game is over
    /// </summary>
    private void GameOver()
    {
        Score = 0;  //reset score
        nextBlockIndex = -1; //next block will be rerandomized

        RemoveMap(); //we remove the map

        mainMenu.SetActive(true); //reactivate the main menu
        Destroy(currentBlock.gameObject); //and destroy the currently falling block
    }
    /// <summary>
    /// currentBlock will be recreated here
    /// </summary>
    private void CreateNewRandomBlock()
    {
        if (currentBlock != null)  //if the currentblock is already a thing
            Destroy(currentBlock.gameObject); //we destroy the old one
        if (nextBlockIndex == -1) //and if the nextblockindex has not been set
            nextBlockIndex = Random.Range(0, blocks.Count); //rerandomize it

        currentBlock = Instantiate(blocks[nextBlockIndex], blockParent).GetComponent<Block>(); //instantiate the block
        currentBlock.Init(this, blockSpeed,(BlockType)nextBlockIndex); //initialize it
        currentBlock.transform.position = blockStart.position; //and set its position to starting position

        nextBlockIndex = Random.Range(0, blocks.Count); //rerandomize the nextblock
        nextBlockText.text = ((BlockType)nextBlockIndex).ToString(); //and update the text to the user
    }

    /// <summary>
    /// when block has stopped moving, this method is called with list of collisions those the block is colliding with (the objects are nodes)
    /// </summary>
    /// <param name="collisions"></param>
    public void BlockDoneFalling(List<GameObject> collisions)
    {
        foreach (var collision in collisions) //for each node we are colliding with
        {
            if (collision.GetComponent<Node>().positionInGrid.x > 20) //if any of them are above the height limit
            {
                AudioManager.Play("GameOver");
                GameOver(); //the game is lost
                return;
            }
                                                                                    //otherwise
            collision.GetComponent<SpriteRenderer>().sprite = blockBlockedSprite;  //set the sprite to mark that this slot in the grid is not in use
            collision.GetComponent<Node>().isActive = true;     //and set the boolean values to mark that it can now collide with upcoming blocks
            collision.GetComponent<Node>().hasBlock = true;     //and aswell as that it now has a block in it
        }

        for (int x = 0; x < nodes[x].Count; x++) //then for each nodes, update the collision detection for the objects above
        {
            for (int y = 0; y < nodes.Count; y++) //loop though all nodes (from down to up starting from line 0, then line 1 etc)
            {
                if (nodes[y][x].GetComponent<Node>().hasBlock && !nodes[y + 1][x].GetComponent<Node>().hasBlock) //if the block we are inspecting has a block in it, and the one above doesnt
                    nodes[y + 1][x].GetComponent<Node>().isActive = true; //, we mark the above one so that it will detect future collisions
            }
        }

        List<int> rowIndexes = new List<int>();  //list to hold indexes of complete rows

        for (int y = 0; y < nodes.Count; y++)    //loop though the nodes again to check if we have complete row
        {
            for (int x = 0; x < nodes[y].Count; x++)
            {
                if (!nodes[y][x].GetComponent<Node>().hasBlock) //if we find a node without block
                    break;      //we break
                else if (x == nodes[y].Count - 1) //and if we make it all the way to the end without breaking
                    rowIndexes.Add(y);  //we found a complete row and add the index of that row to the list
            }
        }

        if(rowIndexes.Count > 0)        //if we score
            AudioManager.Play("Score"); //play sound corresponding to that

        for (int i = 0; i < rowIndexes.Count; i++) //For each new row we found, 
        {

            foreach (var node in nodes[rowIndexes[i]])   //destroy the row
            {
                Destroy(node);
            }
            nodes.Remove(nodes[rowIndexes[i]]); //remove it from the list

            for (int x = rowIndexes[i]; x < nodes.Count; x++)    //And starting from the row
            {
                for (int y = 0; y < nodes[x].Count; y++) //descend all rows above by one 
                {                  
                    nodes[x][y].transform.position = new Vector2(nodes[x][y].transform.position.x, nodes[x][y].transform.position.y - 0.29f);
                    nodes[x][y].GetComponent<Node>().positionInGrid = new Vector2Int(nodes[x][y].GetComponent<Node>().positionInGrid.x - 1, nodes[x][y].GetComponent<Node>().positionInGrid.y);

                    if (x == 0 && !nodes[x][y].GetComponent<Node>().hasBlock) //and if we are on the first row
                        nodes[x][y].GetComponent<Node>().isActive = true; //we make them trigger the collisions
                }
            }

            nodes.Add(new List<GameObject>());  //create new row up top
            for (int y = 0; y < mapSizeX; y++)  
            {
                GameObject node = Instantiate(nodePrefab, nodeParent);
                node.transform.position = new Vector2(y * 0.29f + 0.145f, (mapSizeY - 1) * 0.29f + 0.145f);
                node.GetComponent<Node>().positionInGrid = new Vector2Int(mapSizeY - 1, y);

                nodes[nodes.Count - 1].Add(node);
            }

            for (int index = 0; index < rowIndexes.Count; index++)  //decrement the rowindex, so the other rows those were found, will have correct index
            {
                rowIndexes[index]--;
            }
        }
        Score += rowIndexes.Count * rowIndexes.Count * 10; //add the score
        CreateNewRandomBlock(); //create new random block
    }

    /// <summary>
    /// returns true if the collisions allow movement into the given direction
    /// </summary>
    /// <param name="collisions"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    public bool CheckMovement(List<GameObject> collisions, Vector2Int direction)
    {
        int xOffset = direction.x; //get the x offset
            
        for (int i = 0; i < collisions.Count; i++) //and we check all the collisions
        {
            Vector2Int positionInGrid = collisions[i].GetComponent<Node>().positionInGrid; //get their position in the grid of nodes
            if (nodes[positionInGrid.x].Count <= positionInGrid.y + xOffset || positionInGrid.y + xOffset < 0) //if we would go out of bounds
                return false;   
            if (nodes[positionInGrid.x][positionInGrid.y + xOffset].GetComponent<Node>().hasBlock) //or if we would go into a slot with a block already in it
                return false;   //we return false
        }
        return true; //if we succesfully looped through all the collisions, we return true
    }
    /// <summary>
    /// returns true if the given collisions allow 90 decree rotation into a given direction
    /// </summary>
    /// <param name="collisions"></param>
    /// <param name="rotationDirection"></param>
    /// <returns></returns>
    public bool CheckRotation(List<GameObject> collisions, int rotationDirection)
    {
        Vector2Int centerObject = collisions[0].GetComponent<Node>().positionInGrid; //get the center objects position on the grid

        for (int i = 1; i < collisions.Count; i++) //then loop through all the collisiions
        { 
            Vector2Int positionOnGrid = collisions[i].GetComponent<Node>().positionInGrid; //get the current nodes position

            Vector2Int dirOfCurrentPos = positionOnGrid - centerObject; //get the direciton to the object
            Vector2Int dirToNewPos = new Vector2Int(dirOfCurrentPos.y, dirOfCurrentPos.x) * -rotationDirection; //calculatie the offset to the new slot (from center object, where we would end up)

            if (nodes.Count <= centerObject.x + dirToNewPos.x || centerObject.x + dirToNewPos.x < 0) //if we would go out of bounds in the x axis 
                return false;

            if (nodes[centerObject.y].Count <= centerObject.y + dirToNewPos.y || centerObject.y + dirToNewPos.y < 0)//or if we would go out of bounds in the y axis 
                return false;

            if (nodes[centerObject.x + dirToNewPos.x][centerObject.y + dirToNewPos.y].GetComponent<Node>().hasBlock) //or if we would end up in a slot with another node
                return false;                                                                               //we return false
        }
        return true;//if we succesfully looped through all the collisions, we return true
    }
    /// <summary>
    /// Clears the current map
    /// </summary>
    private void RemoveMap()
    {
        for (int y = 0; y < nodes.Count; y++)
        {
            for (int x = 0; x < nodes[y].Count; x++)
            {
                Destroy(nodes[y][x]); //destroy all nodes
            }
            nodes[y].Clear(); //clear the sublist
        }
        nodes.Clear(); //clear the main list
    }

    /// <summary>
    /// Creates a new map
    /// </summary>
    private void CreateMap()
    {
        for (int y = 0; y < mapSizeY; y++) //for the given sizes
        {
            nodes.Add(new List<GameObject>());
            for (int x = 0; x < mapSizeX; x++)
            {
                GameObject node = Instantiate(nodePrefab, nodeParent);  //create new node
                node.transform.position = new Vector2(x * 0.29f + 0.145f, y * 0.29f + 0.145f); //position it
                node.GetComponent<Node>().positionInGrid = new Vector2Int(y, x); //set its position in the grid
                if (y == 0) //and if we are in the first row
                    node.GetComponent<Node>().isActive = true; //we make it active for collisions
                nodes[y].Add(node); //and add it to the list of nodes
            }
        }
    }
}
