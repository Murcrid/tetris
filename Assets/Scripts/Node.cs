﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// node contains data for the nodes in the grid
/// </summary>
public class Node : MonoBehaviour {

    public bool isActive;   //if we are able to collide with blocks
    public bool hasBlock;   //or if we already have block "inside" us

    public Vector2Int positionInGrid; //and our position in the grid of nodes
}
