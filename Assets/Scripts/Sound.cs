﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

/// <summary>
/// class to store data for sounds to play
/// </summary>
[System.Serializable]
public class Sound {

    [HideInInspector]
    public AudioSource source;

    public string name;

    public AudioClip clip;
    [Range(0.0f, 1.0f)]
    public float volume = 1;
    [Range(0.5f, 3.0f)]
    public float pitch = 1;

    public bool loop;
}
